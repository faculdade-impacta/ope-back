'use strict';

require('dotenv').config();

let fs        = require('fs');
let path      = require('path');
let Sequelize = require('sequelize');
let basename  = path.basename(module.filename);
let env       = process.env.NODE_ENV || 'development';
let db        = {};

let sequelize;
let sequelizeConfig;

if (env === 'production') {
  sequelizeConfig = {
    dbName: process.env.RDS_DB_NAME,
    dbUsername: process.env.RDS_USERNAME,
    dbPass: process.env.RDS_PASSWORD,
    host: process.env.RDS_HOSTNAME,
    port: process.env.RDS_PORT,
    dialect: 'mysql',
    timezone: 'America/Sao_Paulo',
    operatorsAliases: false
  }
    sequelize = new Sequelize(process.env.RDS_DB_NAME, process.env.RDS_USERNAME, process.env.RDS_PASSWORD, {
    host: process.env.RDS_HOSTNAME,
    port: process.env.RDS_PORT,
    dialect: 'mysql',
    timezone: 'America/Sao_Paulo',
    operatorsAliases: false
  });
} else {
  sequelizeConfig = {
    dbName: process.env.DB_NAME, 
    dbUsername: process.env.DB_USERNAME, 
    dbPass: process.env.DB_PASSWORD, 
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'mysql',
    timezone: 'America/Sao_Paulo',
    operatorsAliases: false
  }
    sequelize = new Sequelize(
      process.env.DB_NAME, 
      process.env.DB_USERNAME, 
      process.env.DB_PASSWORD, 
      {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: 'mysql',
        timezone: 'America/Sao_Paulo',
        operatorsAliases: false
      });
}

sequelize.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', sequelizeConfig);
    console.error('Error Log: ', err);
  });

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(function(file) {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;