import models from '../models';

let api = {};

api.getService = (req, res, next) => {
    models.services.belongsTo(models.user, { foreignKey: 'user_id' });
    models.services.findAll({
        where: { user_id: req.user.sub, deleted: false },
        include: [{ model: models.user }]
    }).then((services) => {
        res.send(services);
    }).catch((err) => {
        console.log("ERROR: ", err);
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

api.postService = (req, res, next) => {
    req.body.deleted = false;
    req.body.user_id = req.user.sub;

    models.services.findAll({
        where: { user_id: req.user.sub, deleted: false }
    }).then((services) => {
        if (services.length < 3) {
            req.body.createDate = new Date;
            models.services.create(req.body)
                .then(
                    () => {
                        models.services.belongsTo(models.user, { foreignKey: 'user_id' });
                        models.services.findAll({
                            where: { user_id: req.user.sub, deleted: false },
                            include: [{ model: models.user }]
                        }).then((services) => {
                            res.status(200).send(services);
                        }).catch((err) => {
                            console.log("ERROR: ", err);
                            res.status(err.status || 500);
                            res.json({
                                message: err.message,
                                error: err
                            });
                        });
                    }
                ).catch((err) => {
                    console.log("ERROR: ", err);
                    res.status(err.status || 500);
                    res.json({
                        message: err.message,
                        error: err
                    });
                })
        } else {
            res.status(err.status || 500);
            res.json({
                message: 'Usuário não pode criar mais serviços. Máximo 3.',
                error: err || 500
            });
        }
    }).catch((err) => {
        console.log("ERROR: ", err);
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

api.updateService = (req, res, next) => {
    req.body.updateDate = new Date;
    models.services.update(req.body,
        {
            where: {
                id: req.params.id,
                user_id: req.user.sub
            }
        })
        .then((row) => {
            console.log(row)
            res.sendStatus(200);
        })
        .catch((err) => {
            console.log("ERROR: ", err);
            res.status(err.status || 500);
            res.json({
                message: err.message,
                error: err
            });
        })
}

api.deleteService = (req, res, next) => {
    models.services.update({
        deleted: true,
        updateDate: new Date
    }, { where: { id: req.params.id, user_id: req.user.sub } })
        .then((row) => {
            console.log(row)
            res.sendStatus(200);
        })
        .catch((err) => {
            console.log("ERROR: ", err);
            res.status(err.status || 500);
            res.json({
                message: err.message,
                error: err
            });
        })
}

export default api;