import Joi from 'joi';

export const Search = {
    query: {
        q: Joi.string().required()
    }
}