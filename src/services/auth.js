
import jwkToPem from 'jwk-to-pem';
import jwt from 'express-jwt';
import jwkConfig from '../config/jwk.json';

function verifyJwt() {
    return jwt({
        secret: jwkToPem(jwkConfig.jwkDecryptIdToken),
        credentialsRequired: true,
        getToken: function fromHeaderOrQuerystring (req) {
            if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
                return req.headers.authorization.split(' ')[1];
            } else if (req.query && req.query.token) {
            return req.query.token;
            }
            return null;
        }
    })
}

module.exports = {
    verifyJwt: verifyJwt
}
