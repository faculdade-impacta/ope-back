import models from '../models';
const upload = require('../services/file-upload');
const singleUpload = upload.single('file')

let api = {};

api.getUser = (req, res, next) => {
    models.user.findAll({
        where: {
            id: req.query.id
        }
    }).then((user) => {
        res.send(user);
    }).catch((err) => {
        console.log(err);
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

api.postUser = (req, res, next) => {
    models.user.create({
        'id': req.user.sub,
        'name': req.user.name,
        'email': req.user.email,
        'createDate': new Date
    }).then((row) => {
        console.log('Usu�rio criado com sucesso');
        res.sendStatus(200);
    }).catch((error) => {
        console.log('Usu�rio j� existente');
        res.status(200).send('J� identificado');
    })
}

api.updateUser = (req, res, next) => {
    console.log(req.body)
    models.user.update({
        'name': req.body.name,
        'email': req.body.email,
        'gender': req.body.gender,
        'birthdate': req.body.birthdate,
        'phone': req.body.phone_number,
        'city': req.body['custom:city'],
        'state': req.body['custom:state'],
        'neighborhood': req.body['custom:neighborhood'],
        'address': req.body.address,
        'avatar_id': req.body['custom:avatar_id'],
        'complete_register': req.body['custom:complete_register'],
        'updateDate': new Date
    }, { where: { id: req.user.sub } })
        .then((row) => {
            console.log(row)
            res.sendStatus(200);
        })
        .catch((err) => {
            console.log(err);
            res.status(err.status || 500);
            res.json({
                message: err.message,
                error: err
            });
        })
}

api.postAvatar = (req, res, next) => {
    singleUpload(req, res, function (err, some) {
        if (err) {
            console.log(err)
            return res.status(422).send({ errors: [{ title: 'Image Upload Error', detail: err.message }] });
        }
        return res.json({ 'imageUrl': req.file.location });
    });
}

export default api;