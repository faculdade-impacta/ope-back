import express from 'express';
import validate from 'express-validation';
import serviceController from '../controller/service';

const router = express.Router();

router.get('/', serviceController.getService);
router.post('/', serviceController.postService);
router.put('/:id', serviceController.updateService);
router.delete('/:id', serviceController.deleteService);

module.exports = router;