import express from 'express';
import validate from 'express-validation';
import { Search } from '../services/validation';
import serviceController from '../controller/search';

const router = express.Router();

router.get('/', validate(Search), serviceController.search);
router.get('/public', validate(Search), serviceController.publicSearch);

module.exports = router;