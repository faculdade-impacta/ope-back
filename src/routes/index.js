import express from 'express';

let router = express.Router();

router.get('/', function(req, res, next) {
    res.status(200).json({ data: 'CaaS API: V1.02.02' });
});

module.exports = router;