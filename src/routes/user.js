import express from 'express';
import validate from 'express-validation';
import userController from '../controller/user';

const router = express.Router();

router.get('/', userController.getUser);
router.post('/', userController.postUser);
router.put('/', userController.updateUser);
router.post('/avatar', userController.postAvatar);

module.exports = router;