import models from '../models';
// import region from '../config/estados-cidades.json'

let api = {};

api.search = (req, res, next) => {
    // let city = [];
    // let state = '';
    // let initials = '';
    models.services.belongsTo(models.user, { foreignKey: 'user_id' });
    req.query.l = req.query.l == 'undefined' ? '' : req.query.l;

    // if (req.query.l) {
    //     for (var i = 0; i < region.estados.length; i++) {
    //         if (region.estados[i].nome == req.query.l) {
    //             city = region.estados[i].cidades
    //             state = region.estados[i].nome
    //             initials = region.estados[i].sigla
    //         }
    //     }
    // }

    models.services.findAll({
        attributes: ['categories', 'resume', 'title', 'service_phone', 'service_email'],
        where: {
            resume: { [models.Sequelize.Op.like]: ['%' + req.query.q + '%'] },
            deleted: 0
        },
        include: [{
            model: models.user,
            attributes: ['name', 'state', 'avatar_id', 'city'],
            where: {
                [models.Sequelize.Op.or]: [
                    { city: { [models.Sequelize.Op.like]: ['%' + req.query.l + '%'] } },
                    { state: { [models.Sequelize.Op.like]: ['%' + req.query.l + '%'] } }
                ]
            }
        }]
    }).then((services) => {
        res.status(200).send(services);
    }).catch((err) => {
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });


}

api.publicSearch = (req, res, next) => {
    models.services.belongsTo(models.user, { foreignKey: 'user_id' });
    req.query.l = req.query.l == 'undefined' ? '' : req.query.l;

    models.services.findAll({
        attributes: ['categories', 'resume', 'title'],
        where: {
            resume: { [models.Sequelize.Op.like]: ['%' + req.query.q + '%'] },
            deleted: 0
        },
        include: [{
            model: models.user,
            attributes: ['name', 'state', 'avatar_id', 'city'],
            where: {
                [models.Sequelize.Op.or]: [
                    { city: { [models.Sequelize.Op.like]: ['%' + req.query.l + '%'] } },
                    { state: { [models.Sequelize.Op.like]: ['%' + req.query.l + '%'] } }
                ]
            }
        }]
    }).then((services) => {
        res.send(services);
    }).catch((err) => {
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

export default api;