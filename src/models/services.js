'use strict';
module.exports = (sequelize, DataTypes) => {
    const services = sequelize.define('services', {
        'id': {
            type: DataTypes.UUID,
            primaryKey: true
        },
        'user_id': DataTypes.STRING,
        'categories': DataTypes.TEXT,
        'resume': DataTypes.TEXT,
        'title': DataTypes.TEXT,
        'service_phone': DataTypes.TEXT,
        'service_email': DataTypes.TEXT,
        'deleted': DataTypes.TINYINT,
        'createDate': DataTypes.DATE,
        'updateDate': DataTypes.DATE
    }, {
            timestamps: false,
            underscored: true,
            freezeTableName: true
        });
    return services;
}