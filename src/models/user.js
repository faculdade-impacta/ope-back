module.exports = (sequelize, DataTypes) => {
    let user = sequelize.define('user', {
        'id': {
            type: DataTypes.UUID,
            primaryKey: true
        },
        'name': DataTypes.STRING,
        'surname': DataTypes.STRING,
        'email': DataTypes.STRING,
        'gender': DataTypes.CHAR,
        'birthdate': DataTypes.DATE,
        'phone': DataTypes.INTEGER,
        'city': DataTypes.STRING,
        'state': DataTypes.STRING,
        'neighborhood': DataTypes.STRING,
        'address': DataTypes.STRING,
        'complete_register': DataTypes.TINYINT,
        'avatar_id': DataTypes.STRING,
        'createDate': DataTypes.DATE,
        'updateDate': DataTypes.DATE
    }, {
            timestamps: false,
            underscored: true,
            freezeTableName: true
        });
    return user;
}