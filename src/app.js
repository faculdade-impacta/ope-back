import express from 'express';
import path from 'path';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import fs from 'fs';
import helmet from 'helmet';
import { verifyJwt } from './services/auth';
//remover cors
import cors from 'cors';


let app = express();
require('dotenv').config();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());
app.use(verifyJwt().unless({ path: ['/api/v1/search/public', '/api/v1/'] }));

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", 'http://localhost:3000');
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Authorization, Authorization, Content-Type, Accept");
//     res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    // res.end()
    // next();
// });

/*TODO REMOVER ESSE CORS E MANTER O GATEWAY ANTERIOR*/
app.use(cors());

/** 
 * ROTEAMENTO 
 * */
fs.readdirSync(__dirname + '/routes')
  .forEach((file) => {
    if (file.indexOf('.') == 0 ) return;
    let route = require('./routes/' + file);
    if ( file === 'index.js') {
      app.use('/api/v1', route)
    } else {
      app.use("/api/v1/"+file.substring(0, file.indexOf('.')), route)
    }
});
/**  
 * 
 * error handlers
 * 
 * */
//error when the token is invalid
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  }
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err
  });
});

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    console.log(err.message)
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;

